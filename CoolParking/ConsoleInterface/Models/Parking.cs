﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text;

namespace ConsoleInterface.Models
{
    public class Parking
    {
        [JsonProperty("balance")]
        public decimal Balance { get; set; }
        [JsonProperty("capacity")]
        public int Capacity { get; set; }
        [JsonProperty("freePlaces")]
        public int FreePlaces { get; set; }
    }
}
