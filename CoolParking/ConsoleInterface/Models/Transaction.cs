﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleInterface.Models
{
    class Transaction
    {
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }
        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }
    }
}
