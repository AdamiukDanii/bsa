﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Validators;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {

        private ITimerService withdrawTimer;
        private ITimerService logTimer;
        private ILogService logService;
        Parking parking;
        CompositeValidator validator = new CompositeValidator();
        Transactions transactions = new Transactions();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            parking = Parking.getParkingInstance;
            this.withdrawTimer = withdrawTimer;
            this.withdrawTimer.Interval = Settings.PaymentPeriod;
            this.withdrawTimer.Elapsed += new ElapsedEventHandler(parking.Withdraw);
            this.withdrawTimer.Start();

            this.logTimer = logTimer;
            this.logTimer.Interval = Settings.LoggingPeriod;
            this.logTimer.Elapsed += new ElapsedEventHandler(transactions.WriteLog);
            this.logTimer.Start();
            this.logService = logService;

        }


        public void AddVehicle(Vehicle vehicle)
        {

            if (new IsFreePlaceExist(parking).Validate() == false)
            {
                throw new InvalidOperationException("There is no place in the parking");
            }
            else if (new IsVehicleExist(parking, vehicle.Id).Validate() == true)
            {
                throw new ArgumentException("This car is already in the parking");
            }
            else if (new IsIdFormatOk(vehicle.Id).Validate() == false)
            {
                throw new ArgumentException("Input data is invalid");
            }
            else
            {
                parking.CurrentParkingCapacity--;
                parking.ParkedVehicles.Add(vehicle);
            }



        }

        public void Dispose()
        {
            parking.ParkedVehicles.Clear();
            parking.CurrentParkingCapacity = Settings.ParkingCapacity;
            parking.CurrentParkingBalance = Settings.InitialParkingBalance;
            withdrawTimer.Dispose();
            logTimer.Dispose();
            GC.SuppressFinalize(this);
        }

        public decimal GetBalance()
        {
            return parking.CurrentParkingBalance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return parking.CurrentParkingCapacity;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Transactions.LastParkingTransactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return parking.ParkedVehicles.AsReadOnly();
        }

        public string ReadFromLog()
        {
            LogService logService = new LogService(Settings.logFilePath);
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (new IsIdFormatOk(vehicleId).Validate() == false)
            {
                throw new ArgumentException("Input data is invalid");
            }
            else if (new IsVehicleExist(parking, vehicleId).Validate() == false)
            {
                throw new InvalidOperationException("Vehicle is not exist");
            }
            else if (new IsVehicleBalancePositive(parking, vehicleId).Validate() == false)
            {
                throw new InvalidOperationException("This car has debt");
            }
            else
            {
                var vehicleToDelete = parking.ParkedVehicles.Find(v => v.Id == vehicleId);
                parking.CurrentParkingCapacity++;
                parking.ParkedVehicles.Remove(vehicleToDelete);

            }

        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            
            validator.Add(new IsVehicleExist(parking, vehicleId));
            if (sum < 1 && new IsIdFormatOk(vehicleId).Validate() == false)
            {
                throw new ArgumentException("Input data is invalid");
            }
            else if (new IsVehicleExist(parking, vehicleId).Validate() == false)
            {
                throw new InvalidOperationException("Vehicle is not exist");
            }
            else 
            {
                for (int i = 0; i < parking.ParkedVehicles.Count; i++)
                {
                    if (parking.ParkedVehicles[i].Id == vehicleId) parking.ParkedVehicles[i].Balance += sum;
                }
            }

        }


    }
}
