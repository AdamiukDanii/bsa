﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {

        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed
        {
            add { timer.Elapsed += value; }
            remove { timer.Elapsed -= value; }
        }

        public Timer timer = new Timer();


        public void Dispose()
        {
            
            timer.Dispose();
            GC.SuppressFinalize(this);
        }

        public void Start()
        {
            timer.Interval = Interval; 
            timer.AutoReset = true;
            timer.Start();
           

        }

        public void Stop()
        { 
            timer.Stop();
            Dispose();
        }



    }
}