﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
//       Items: PassengerCar, Truck, Bus, Motorcycle.
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public decimal Sum { get; set; }
        public string Id { get; set; }
        public DateTime TransactionDate { get; set; }
        
        



    }
}