﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Validators
{
    class IsIdFormatOk : IValidate
    {
        string _id;
        public IsIdFormatOk(string id)
        {
            _id = id;
        }
        public bool Validate()
        {
            Regex regex = new Regex("[A-Z]{2}-[0-9]{4}-[A-Z]{2}"); //Regex for license plate AA-0000-AA format
            return regex.IsMatch(_id);
        }
    }
}
