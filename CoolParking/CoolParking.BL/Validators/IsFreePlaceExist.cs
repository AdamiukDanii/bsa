﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Validators
{
    class IsFreePlaceExist : IValidate
    {
        Parking _parking;
        public IsFreePlaceExist(Parking parking)
        {
            _parking = parking;
        }
        public bool Validate()
        {
            return _parking.CurrentParkingCapacity > 0;
        }
    }
}
