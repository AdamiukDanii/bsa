﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Validators
{
    interface IValidate
    {
        bool Validate();
    }
}
