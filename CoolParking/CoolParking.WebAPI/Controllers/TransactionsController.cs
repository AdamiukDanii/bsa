﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private IParkingService parkingService;
        public TransactionsController(IParkingService service)
        {
            parkingService = service;
        }
        // GET: api/transactions/last
        [HttpGet]
        [Route("last")]
        public ActionResult<TransactionInfo[]> GetGetLastParkingTransactions()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetLastParkingTransactions()));
        }

        // GET: api/transactions/all
        [HttpGet]
        [Route("all")]
        public ActionResult<string> GetAllParkingTransactions()
        {
            try
            {
                return Ok(JsonConvert.SerializeObject(parkingService.ReadFromLog()));
            }
            catch(Exception ex) { return NotFound(JsonConvert.SerializeObject(ex.Message)); }
        }

        // PUT: api/transactions/topUpVehicle
        [HttpPut]
        [Route("topUpVehicle")]
        public ActionResult TopUpVehicle([FromBody]TransactionInfo transaction)
        {
            try
            {
                parkingService.TopUpVehicle(transaction.Id, transaction.Sum);
                return Ok();
            }
            catch(ArgumentException ex)
            {
                return BadRequest(JsonConvert.SerializeObject(ex.Message));
            }
            catch(InvalidOperationException ex)
            {
                return NotFound(JsonConvert.SerializeObject(ex.Message));
            }
            
        }

        
    }
}
