﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService parkingService;
        public ParkingController(IParkingService service)
        {
            parkingService = service;
        }
        // GET: api/parking/balance
        [HttpGet]
        [Route("balance")]
        public ActionResult<decimal> GetParkingBalance()
        {
            
            return Ok(JsonConvert.SerializeObject(parkingService.GetBalance()));
        }

        // GET: api/Parking/capacity
        [HttpGet]
        [Route("capacity")]
        public ActionResult<int> GetParkingCapacity()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetCapacity()));
        }

        // GET: api/Parking/freePlaces
        [HttpGet]
        [Route("freePlaces")]
        public ActionResult<int> GetParkingFreePlaces()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetFreePlaces()));
        }

       
    }
}
